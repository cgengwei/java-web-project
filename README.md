# java-web-project

#### 介绍
javaWeb最简单的项目结构

#### 软件架构
软件架构说明

这个小demo是为了配合 博文：[javaWeb项目结构](https://www.cnblogs.com/cgengwei/p/14054774.html)讲解一个javaWeb项目最简单的结构应该是怎么样的。

classes下的java文件也不是必要的，只是为了让大家指导对应的class文件是用哪个java文件编译出来的。


#### 使用说明

1. 直接将这个文件夹置于tomcat的webapp文件夹下。
2. 然后访问 ttp://localhost:8080/MyWebApp/myservlet 即可得到结果。





